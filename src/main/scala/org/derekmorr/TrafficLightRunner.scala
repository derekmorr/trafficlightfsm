package org.derekmorr

import scala.concurrent.duration._
import scala.concurrent.duration.Duration.Zero
import scala.language.postfixOps

import akka.actor.ActorSystem
import org.derekmorr.TrafficLight.Tick

object TrafficLightRunner extends App {

  val actorSystem = ActorSystem.create("TrafficLightDemo")
  val trafficLight = actorSystem.actorOf(TrafficLight.props, "traffic-light")

  val scheduler = actorSystem.scheduler
  scheduler.schedule(Zero, 100 milliseconds, trafficLight, Tick)(actorSystem.dispatcher)

  Thread.sleep(5000)

  actorSystem.shutdown()
}
