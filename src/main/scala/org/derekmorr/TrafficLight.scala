package org.derekmorr

import akka.actor.{Actor, ActorLogging, FSM, Props}
import org.derekmorr.TrafficLight._

/**
 * Toy Finite State Machine for a traffic light
 */
class TrafficLight extends Actor with ActorLogging with FSM[Colors, DummyData] {

  startWith(Red, NoData)
  log.info("initialized to red")

  when(Red) {
    case Event(Tick, NoData) => goto(Green) using NoData
  }

  when(Yellow) {
    case Event(Tick, NoData) => goto(Red) using NoData
  }

  when(Green) {
    case Event(Tick, NoData) => goto(Yellow) using NoData
  }

  onTransition {
    case Red -> Green 		=> log.info("Red -> Green")
    case Green -> Yellow 	=> log.info("Green -> Yellow")
    case Yellow -> Red		=> log.info("Yellow -> Red")
  }

  initialize()
}

object TrafficLight {

  // states
  sealed trait Colors
  case object Red extends Colors
  case object Yellow extends Colors
  case object Green extends Colors

  // data we track
  sealed trait DummyData
  case object NoData extends DummyData

  case object Tick

  // akka-specific factory method; safe to ignore this for this demo.
  def props: Props = Props(new TrafficLight)
}
