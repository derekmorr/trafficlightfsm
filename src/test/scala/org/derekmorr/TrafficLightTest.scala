package org.derekmorr

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestFSMRef}
import org.derekmorr.TrafficLight._
import org.scalatest.{BeforeAndAfterAll, WordSpecLike, MustMatchers}

/**
 * Unit tests for TrafficLight
 */
class TrafficLightTest extends TestKit(ActorSystem("testsystem")) with MustMatchers with WordSpecLike with BeforeAndAfterAll {

  "TrafficLight" must {

    "initialize to red" in {
      val testLight = TestFSMRef(new TrafficLight)
      testLight.stateName mustEqual Red
    }

    "transition from red to green" in {
      val testLight = TestFSMRef(new TrafficLight)
      testLight.setState(Red, NoData)

      testLight ! Tick

      testLight.stateName mustEqual Green
    }

    "transition from green to yellow" in {
      val testLight = TestFSMRef(new TrafficLight)
      testLight.setState(Green, NoData)

      testLight ! Tick

      testLight.stateName mustEqual Yellow
    }

    "transition from yellow to red" in {
      val testLight = TestFSMRef(new TrafficLight)
      testLight.setState(Yellow, NoData)

      testLight ! Tick

      testLight.stateName mustEqual Red
    }
  }

  /** shutdown the test actor system after all tests finish. */
  override protected def afterAll {
    super.afterAll
    system.shutdown
  }
}
