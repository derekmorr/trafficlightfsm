# TrafficLightFSM

This is a toy implementation of a Traffic Light using the Akka [Finite State Machine DSL](http://doc.akka.io/docs/akka/2.3.9/scala/fsm.html). It also has basic tests using the [Akka TestKit](http://doc.akka.io/docs/akka/2.3.9/scala/testing.html).

# Prerequisites

You will need [Java installed](http://java.oracle.com/). The app will download any additional dependencies.

# Running the app:

From the shell

    ./sbt run
    
# Running tests

From the shell

    ./sbt test

# Manually installing dependencies

The project depends on [Scala](http://www.scala-lang.org/) and its build tool, [sbt](http://www.scala-sbt.org/). If the `sbt` script fails to install Scala and SBT, you can manually install them. 

On OS X, assuming you have [homebrew](http://brew.sh/) installed, the easiest way to install these is via:

    brew install scala sbt
    
Alternatively, you can download native OS packages from the links above.